# Copyright 2019 Vauxoo
# License AGPL-3 or later (http://www.gnu.org/licenses/agpl).
{
    'name': "Res Partner Bank AR",
    'summary': """
        Res partner Bank AR for Cotesma""",
    'author': "Vauxoo",
    'website': "http://www.cotesma.coop",
    'category': 'Argentina',
    'license': 'AGPL-3',
    'version': '12.0.1.0.0',
    'depends': ['base'],
    'data': ['views/res_partner_bank.xml'],
    'demo': [
    ],
    'installable': True,
    'auto_install': False,
    'application': True,
}
