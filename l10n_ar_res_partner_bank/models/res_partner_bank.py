# pylint: disable=eval-used
# pylint: disable=eval-referenced
# pylint: disable=consider-add-field-help
# pylint: disable=broad-except

import logging

from odoo import models, fields, api, _
from odoo.exceptions import ValidationError

_logger = logging.getLogger(__name__)


class ResPartnerBank(models.Model):
    _inherit = 'res.partner.bank'

    acc_cbu = fields.Char('Account CBU', size=22, required=True)

    @api.constrains('acc_cbu')
    def _check_cbu(self):
        for record in self:
            if len(record.acc_cbu) < 22:
                raise ValidationError(_("CBU must have 22 numbers"))
